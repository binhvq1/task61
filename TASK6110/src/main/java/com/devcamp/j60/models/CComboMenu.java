package com.devcamp.j60.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "combomenu")
public class CComboMenu {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @Column(name = "name")
    private String name = "";

    @Column(name = "duongkinh")
    private int duongkinh = 0;

    @Column(name = "suonnuong")
    private int suonnuong = 0;

    @Column(name = "salad")
    private int salad = 0;
    @Column(name = "nuocngot")
    private int nuocngot = 0;

    @Column(name = "thanhtien")
    private int thanhtien = 0;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDuongkinh() {
        return duongkinh;
    }

    public void setDuongkinh(int duongkinh) {
        this.duongkinh = duongkinh;
    }

    public int getSuonnuong() {
        return suonnuong;
    }

    public void setSuonnuong(int suonnuong) {
        this.suonnuong = suonnuong;
    }

    public int getSalad() {
        return salad;
    }

    public void setSalad(int salad) {
        this.salad = salad;
    }

    public int getNuocngot() {
        return nuocngot;
    }

    public void setNuocngot(int nuocngot) {
        this.nuocngot = nuocngot;
    }

    public int getThanhtien() {
        return thanhtien;
    }

    public void setThanhtien(int thanhtien) {
        this.thanhtien = thanhtien;
    }

    public CComboMenu() {
    }

    public CComboMenu(int id, String name, int duongkinh, int suonnuong, int salad, int nuocngot, int thanhtien) {
        this.id = id;
        this.name = name;
        this.duongkinh = duongkinh;
        this.suonnuong = suonnuong;
        this.salad = salad;
        this.nuocngot = nuocngot;
        this.thanhtien = thanhtien;
    }

}
