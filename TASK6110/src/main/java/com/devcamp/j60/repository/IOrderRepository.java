package com.devcamp.j60.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j60.models.*;

public interface IOrderRepository extends JpaRepository<COrder, Long> {
}
