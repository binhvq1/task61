package com.devcamp.j60.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j60.models.CCustomer;

public interface ICustomerRepository extends JpaRepository<CCustomer, Long> {
    CCustomer findCustomerByUserCode(String user_code);
}