package com.devcamp.j60.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j60.models.CVoucher;

public interface IVoucherRepository extends JpaRepository<CVoucher, Long> {

}
