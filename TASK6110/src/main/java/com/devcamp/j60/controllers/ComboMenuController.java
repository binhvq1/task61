package com.devcamp.j60.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j60.models.CComboMenu;
import com.devcamp.j60.repository.IComboMenuRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class ComboMenuController {
    @Autowired
    private IComboMenuRepository comboMenuRepository;

    @GetMapping(value = "/combomenu")
    public ResponseEntity<List<CComboMenu>> getAllVouchers() {
        try {
            List<CComboMenu> pComboMenu = new ArrayList<CComboMenu>();

            comboMenuRepository.findAll().forEach(pComboMenu::add);

            return new ResponseEntity<>(pComboMenu, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
