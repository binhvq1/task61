package com.devcamp.j60.controllers;

import java.lang.StackWalker.Option;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j60.models.CCustomer;
import com.devcamp.j60.models.COrder;
import com.devcamp.j60.repository.ICustomerRepository;
import com.devcamp.j60.repository.IOrderRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class OrderController {
    @Autowired
    private ICustomerRepository customerRepository;

    @GetMapping(value = "/devcamp-orders")

    public ResponseEntity<Set<COrder>> getAllCarTypes(@RequestParam(value = "userId") Long userId) {
        try {
            // Long id = Long.parseLong(userId);
            Optional<CCustomer> vCustomer = customerRepository.findById(userId);
            if (vCustomer != null) {
                return new ResponseEntity<>(vCustomer.get().getOrders(), HttpStatus.OK);

            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);

            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);

        }
    }
}
