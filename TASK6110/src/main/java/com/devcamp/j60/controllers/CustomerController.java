package com.devcamp.j60.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j60.models.CCustomer;
import com.devcamp.j60.repository.ICustomerRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class CustomerController {
    @Autowired
    private ICustomerRepository customerRepository;

    @GetMapping("/customers")
    public ResponseEntity<List<CCustomer>> getAllCustomer() {
        try {
            List<CCustomer> pCustomer = new ArrayList<CCustomer>();

            customerRepository.findAll().forEach(pCustomer::add);

            return new ResponseEntity<>(pCustomer, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
