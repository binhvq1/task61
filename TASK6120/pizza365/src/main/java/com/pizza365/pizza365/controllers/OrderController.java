package com.pizza365.pizza365.controllers;

import java.util.Optional;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.pizza365.pizza365.model.CCustomer;
import com.pizza365.pizza365.model.COrder;
import com.pizza365.pizza365.repository.ICustomerRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class OrderController {
    @Autowired
    private ICustomerRepository customerRepository;

    @GetMapping("/orders")

    public ResponseEntity<Set<COrder>> getAllOrdersByCustomerId(@RequestParam(required = true) Long userId) {
        try {
            Optional<CCustomer> vCustomer = customerRepository.findById(userId);
            if (vCustomer != null) {
                return new ResponseEntity<>(vCustomer.get().getOrders(), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
