package com.pizza365.pizza365.controllers;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.pizza365.pizza365.model.CMenu;
import com.pizza365.pizza365.repository.IMenuRepository;

@RestController
@CrossOrigin
@RequestMapping("/")
public class MenuController {
    @Autowired
    private IMenuRepository menuRepository;

    @GetMapping(value = "/menus")
    public ResponseEntity<List<CMenu>> getAllMenus() {
        try {
            List<CMenu> pMenu = new ArrayList<CMenu>();

            menuRepository.findAll().forEach(pMenu::add);

            return new ResponseEntity<>(pMenu, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
